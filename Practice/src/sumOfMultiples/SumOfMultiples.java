package sumOfMultiples;

public class SumOfMultiples {

	/**
	 * Given a number, find the sum of all multiples of 3 & 5.
	 * 
	 * Example: sumOfAll3And5Multiples(100);
	 * 
	 * @param n
	 */
	public void sumOfAll3And5Multiples(int n) {
		int sum = 0;
		for (int i = 3; i <= n; i += 3) {
			sum += i;
		}
		for (int i = 5; i <= n; i += 5) {
			sum += i;
		}
		System.out.println(sum);
	}

}

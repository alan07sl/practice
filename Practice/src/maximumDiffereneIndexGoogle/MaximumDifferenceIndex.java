package maximumDiffereneIndexGoogle;

public class MaximumDifferenceIndex {

	/**
	 * Given an array A of integers, find the maximum of j - i subjected to the
	 * constraint of A[i] <= A[j].
	 * 
	 * Simple Solution. Inefficient.
	 * 
	 * Example: maximumIndex(new int[] { 3, 5, 4, 2 }); Output: 2
	 * 
	 * @param ints
	 */
	public void maximumIndex(int[] ints) {
		int maxDiff = 0;
		int n = ints.length - 1;
		for (int i = 0; i < n; i++) {
			for (int j = n; j > i; j--) {
				if (ints[i] <= ints[j] && j - i > maxDiff) {
					maxDiff = j - i;
				}
			}
		}

		System.out.println(maxDiff);
	}

	/**
	 * 
	 * More efficent way.
	 * 
	 * maximumIndexEfficent(new int[] { 3, 5, 4, 2 });
	 * 
	 * @param ints
	 */
	public static void maximumIndexEfficent(int[] ints) {
		int maxDiff = 0;
		int n = ints.length;

		int RMax[] = new int[n];
		int LMin[] = new int[n];

		LMin[0] = ints[0];
		for (int i = 1; i < n; ++i) {
			LMin[i] = min(ints[i], LMin[i - 1]);
		}

		RMax[n - 1] = ints[n - 1];
		for (int j = n - 2; j >= 0; --j) {
			RMax[j] = max(ints[j], RMax[j + 1]);
		}

		int i = 0, j = 0;

		while (j < n && i < n) {
			if (LMin[i] < RMax[j]) {
				maxDiff = max(maxDiff, j - i);
				j = j + 1;
			} else {
				i = i + 1;
			}
		}
		System.out.println(maxDiff);
	}

	private static int min(int i, int j) {
		return i < j ? i : j;
	}

	private static int max(int i, int j) {
		return i > j ? i : j;
	}

}

package binaryTree;

public class BinarySearchTree extends Tree {

	@Override
	protected int maxElem(Node node) {
		if (node.right == null) {
			return node.element;
		} else {
			return maxElem(node.right);
		}
	}

}

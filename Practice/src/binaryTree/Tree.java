package binaryTree;

public abstract class Tree {

	protected class Node {
		protected Integer element;
		protected Node left;
		protected Node right;

		Node(int element) {
			this.element = element;
			left = right = null;
		}

		Node(int element, Node left, Node right) {
			this.element = element;
			this.left = left;
			this.right = right;
		}

	} // end Node class

	public class NodeReference {
		private Node node;

		private NodeReference(Node node) {
			this.node = node;
		}

		public int getElement() {
			return node.element;
		}

		public void setElement(int e) {
			node.element = e;
		}
	}

	protected Node root;

	public Tree() {
		root = null;
	}

	public int maxElem() {
		if (root == null)
			throw new IllegalStateException("Empty tree.");
		return maxElem(root);
	}

	protected abstract int maxElem(Node node);
	
}
